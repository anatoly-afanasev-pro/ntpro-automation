# ВНИМАНИЕ!
#
# Это не стабилизированное API, скорее прототип того что такое API возможно
# Использование такого API в боевой версии, если оно понадобится, нужно отдельно прорабатывать

import base64
import json
import time
import urllib.parse
from typing import Optional

from constants import NTPRO_PORTAL_HOST, NTPRO_USER, NTPRO_PASSWORD
from utils import DateRange, set_range_week, HTTPClientConnection


TAB_NAME = 'clients_toxicity_grouped_by_client'
NTPRO_PORTAL_REPORT_URL = '/ecn/clients_toxicity_grouped_data/'

ACCOUNT_ID_COLUMN = 'tr_account_id'
AVG_PL_COLUMN = 'avg_pl1'


class PortalApi:
    @staticmethod
    def get(from_date, to_date):
        auth_params = PortalHttpAuthorization.qt_auth_params()
        auth_params.update({
            'datasource': 1,
            'trade_time__startDate': from_date,
            'trade_time__endDate': to_date,
        })

        params = urllib.parse.urlencode(auth_params)
        url = f"{NTPRO_PORTAL_REPORT_URL}?{params}"

        data = {}

        with HTTPClientConnection(NTPRO_PORTAL_HOST, secure=True) as conn:
            print(f"GET https://{NTPRO_PORTAL_HOST}{url}")

            conn.request("GET", url, None, {'X-Requested-With': 'XMLHttpRequest'})
            response = conn.getresponse()
            if response.status != 200:
                if response.status == 401:
                    print("Error: Bad authentication data")
                else:
                    print(f"Error: Bad response status {response.status}")
                return data

            print('response status 200 OK')
            data_response = response.read()

        response_json = json.loads(data_response)
        data = response_json['dataTable'][TAB_NAME]['fetchedRows']

        return data

    @staticmethod
    def get_toxic_account_ids(date_range: Optional[DateRange] = None) -> set:
        date_range = date_range or set_range_week()
        groups = PortalApi.get(date_range.start, date_range.finish)

        result = {group[ACCOUNT_ID_COLUMN] for group in groups if float(group[AVG_PL_COLUMN]) < 0}

        return result


class PortalHttpAuthorization:
    SIGN_AUTH_SALT = "^NTProgress$"
    PARAM_TIMESTAMP = "_trequest_timestamp"
    PARAM_USERNAME = "_trequest_username"
    PARAM_SIGN = "_trequest_sign"

    @staticmethod
    def hasher(password):
        import sha3
        encoder = sha3.keccak_512()
        encoder.update(password.encode('utf8'))
        my_hash = base64.standard_b64encode(encoder.digest())
        return my_hash.decode('utf8')

    @staticmethod
    def get_sign(timestamp, username, password_hash):
        sign_source = timestamp + PortalHttpAuthorization.SIGN_AUTH_SALT + username + password_hash
        return PortalHttpAuthorization.hasher(sign_source)

    @staticmethod
    def qt_auth_params(username=NTPRO_USER, password=NTPRO_PASSWORD, timestamp_dt=None):
        if timestamp_dt is None:
            timestamp = int(time.time())
        else:
            timestamp = timestamp_dt.timestamp()

        timestamp = str(timestamp)

        sign = PortalHttpAuthorization.get_sign(timestamp, username, PortalHttpAuthorization.hasher(password))

        return {
            PortalHttpAuthorization.PARAM_SIGN: sign,
            PortalHttpAuthorization.PARAM_USERNAME: username,
            PortalHttpAuthorization.PARAM_TIMESTAMP: timestamp
        }
