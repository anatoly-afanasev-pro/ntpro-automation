#!/usr/bin/env python3

import os
import shutil
import subprocess


PROJECT_ROOT = os.getcwd()
venv_path = os.path.join(PROJECT_ROOT, 'venv')

skip_venv = False
if os.path.exists(venv_path):
    print(f'found existing virtual env at {venv_path}. use it?')
    answer = input('yes/no? ')
    if answer == 'yes':
        skip_venv = True
    else:
        shutil.rmtree(venv_path)

if not skip_venv:
    print(f'installing virtualenv {venv_path}')
    subprocess.check_call(['python', '-m', 'venv', venv_path])
else:
    print(f'using virtual env {venv_path}')

requirements = os.path.join(PROJECT_ROOT, 'requirements.txt')

pip_path = os.path.join(venv_path, 'bin/pip3')
if not os.path.exists(pip_path):
    pip_path = os.path.join(venv_path, 'Scripts/pip3.exe')

command = [pip_path, 'install', '-r', requirements]
print(' '.join(command))
subprocess.check_call([pip_path, 'install', '-r', requirements])
