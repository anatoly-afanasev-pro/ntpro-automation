import json
from dataclasses import dataclass, field
from enum import Enum
from typing import Dict


class NTProMessageType(int, Enum):
    PING_REQUEST = 1
    PING_REPLY = 2
    DATA_MESSAGE = 3


class NTProRequestType(str, Enum):
    Hello = 'Hello'
    Environment = 'Environment'
    LoginRequest = 'LoginRequest'
    LoginReply = 'LoginReply'
    SelectUserRoleRequest = 'SelectUserRoleRequest'
    CommandReply = 'CommandReply'
    RequestAccountStates = 'RequestAccountStates'
    AccountStatesUpdate = 'AccountStatesUpdate'
    ChangeAccountRequest = 'ChangeAccountRequest'


@dataclass
class NTProMessage:
    Type: NTProMessageType
    Msg: str

    def to_json(self):
        return {
            'Type': self.Type,
            'Msg': self.Msg,
        }

    @staticmethod
    def from_json(json_data: Dict):
        json_data['Type'] = NTProMessageType(json_data['Type'])

        if json_data['Type'] == NTProMessageType.DATA_MESSAGE:
            json_data.update(json.loads(json_data['Msg']))
            json_data['Msg'] = json_data['Message']
            del json_data['Message']

            return NTProDataMessage(**json_data)

        return NTProMessage(**json_data)


@dataclass
class NTProDataMessage:
    RequestId: int
    OriginalRequestId: int
    RequestType: NTProRequestType

    Msg: dict = field(default_factory=dict)
    Type: NTProMessageType = NTProMessageType.DATA_MESSAGE

    def to_json(self):
        msg = {
            "RequestId": self.RequestId,
            "OriginalRequestId": self.OriginalRequestId,
            "RequestType": self.RequestType,
            "Message": self.Msg
        }

        return {
            'Type': self.Type,
            'Msg': json.dumps(msg),
        }
