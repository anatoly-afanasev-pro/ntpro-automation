import datetime
import http.client
from typing import NamedTuple

DATE_TIME_MINUTES_FORMAT = '%Y.%m.%d %H:%M'


class DateRange(NamedTuple):
    start: str
    finish: str


def set_range_week():
    now = datetime.datetime.now()
    return DateRange((now - datetime.timedelta(days=7)).strftime(DATE_TIME_MINUTES_FORMAT),
                     now.strftime(DATE_TIME_MINUTES_FORMAT))


def ask(variants, prefix=None):
    help_text = "\n".join([f"{i}: {v}" for i, v in variants.items()])
    if prefix:
        print(f"{prefix}\n")

    while True:
        user_input = input(f'Please select action:\n{help_text}\n')
        if user_input in variants:
            break
        print("Input unknown")

    return user_input


class HTTPClientConnection(object):
    def __init__(self, host, secure=False):
        self.secure = secure
        self.host = host
        self.conn = None

    def enter(self):
        if self.secure:
            self.conn = http.client.HTTPSConnection(self.host)
        else:
            self.conn = http.client.HTTPConnection(self.host)
        return self.conn

    def __enter__(self):
        return self.enter()

    def exit(self, exc_type, exc_val, exc_tb):
        self.conn.close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self.exit(exc_type, exc_val, exc_tb)
