NTPRO_USER = 'manager'
NTPRO_PASSWORD = 'manager-password'

NTPRO_PORTAL_HOST = 'ntportal-uat.ntprog.com'
NTPRO_WS_URL = 'wss://public-api-uat.ntprog.com'

PUBLIC_WS_API_MAJOR_VERSION = 2
PUBLIC_WS_API_MINOR_VERSION = 4
