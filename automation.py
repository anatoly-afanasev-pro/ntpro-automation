import asyncio
import sys
import logging

from utils import ask
from portal_http_api import PortalApi  # API для доступа к данным поратала
from ws_api import WSApi  # API для доступа к websocket протоколу NTPro

ws_api = WSApi()
toxic_account_ids = {}

logging.basicConfig(stream=sys.stdout,
                    level=logging.DEBUG,
                    format='[%(asctime)s: %(levelname)s] %(message)s')

while True:
    toxic_info = None
    if toxic_account_ids:
        toxic_info = f"Toxic account ids: {toxic_account_ids}"

    action = ask({
        '1': 'get toxic clients',
        '2': 'input toxic clients manually',
        '3': 'set toxic client special executor group',
        'q': 'quit'
    }, toxic_info)

    if action == 'q':
        break

    if action == '1':
        toxic_account_ids = PortalApi.get_toxic_account_ids()
    elif action == '2':
        toxic_account_ids_string = input("Input toxic accounts id's as comma separated list (ex. 1,2,3): ")
        ids = toxic_account_ids_string.split(',')
        if not ids:
            print("Bad input. Do you use comma: ,")
            continue

        try:
            toxic_account_ids = {int(account_id) for account_id in ids}
        except ValueError:
            print(f"Cannot parse {toxic_account_ids_string} into numbers")
            continue
    elif action == '3':
        if not toxic_account_ids:
            print(f"Found no toxic clients. Get it from portal or input manually (1 or 2)")
            continue

        group_id = input("Input toxic group id: ")
        try:
            group_id = int(group_id)
        except ValueError:
            print(f"Cannot parse {group_id} into number")
            continue

        asyncio.run(ws_api.update_executor_group(toxic_account_ids, group_id))
